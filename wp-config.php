<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bayuslar_wp12');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lksj36et7botfmytxa1r9xc4j8lo2mcarg9rzfwwu99bovzxd2ey4qawaugf2mfa');
define('SECURE_AUTH_KEY',  'o3utpbpdciou6snvdcw7ppn6maiji9pnoymx67wxzaoehavr8lh8be1geaowwew8');
define('LOGGED_IN_KEY',    'wpt2a4ugypbohg7x66uny54dbaz9awzjvjsww5pp8kxx8mkbhgrevfdjfolhdauv');
define('NONCE_KEY',        'e512d8kqry7agm3inwgaknxfvfgf6swatftmotu2pwarcfpfnm19kvmuc279jweo');
define('AUTH_SALT',        'bfkncegzh75fvzvpx2vxcatgqmb4hqfurdhxglq8iydvikpx7yg6ebqgnft5dkos');
define('SECURE_AUTH_SALT', '6wlc0eut5lkhyo7nroi60znjshktdnh2jsp3jusyfmvbvel7n9f9y2bbfstfnozz');
define('LOGGED_IN_SALT',   'zvbill22wm3ezawf6y0hklvorhprnokfvy37xccjmkx7pask8ex6juhsnjluqsxp');
define('NONCE_SALT',       'rm43jxcxynuerzumhxcnhrnfi1q7nnp7buggtpgksdaiwedhry1z5va08dgdiwe0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpjv_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
