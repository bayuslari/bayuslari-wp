<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Casper
 */
?>
	<footer id="colophon" class="site-footer" role="contentinfo">
	    <a class="show-widgets icon-slari-settings" href="#"><span class="tooltip"><?php _e('Show widgets, please!'); ?></span></a>
		<div class="site-info inner">
		    <section class="copyright">
		    	<?php if(  false == get_theme_mod( 'casper_custom_footer') ) { ?>
		    		<?php printf( __( 'from <a href="%1$s" rel="home">Casper WP</a> and customized with ❤ by <a href="https://github.com/bayuslari">Bayu Slari</a>'), esc_url( 'https://github.com/lacymorrow/casper' ) ); ?>
		    	<?php } else { echo get_theme_mod( 'casper_custom_footer'); } ?>
		    </section>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</main><!-- /#content -->

<?php wp_footer(); ?>
</body>
</html>