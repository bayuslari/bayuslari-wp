/* =============================
    TABLE OF CONTENTS
    ============================
    1. SEARCH FORM
================================== */

var script = {

    // ======================================================================
    // Your function here
    // * Don't forget to use proper function name to describes your function
    // ======================================================================

    searchForm: function() {

        // Init and restructuring HTML
        jQuery('#masthead, #content').wrapAll('<div class="wrapper" />');
        jQuery('<li><a class="search-icon"><i class="icon-slari-zoom"></i></a></li>').appendTo('header .nav-menu');
        jQuery('.widget_search').appendTo('body');
        jQuery('<a class="close-pop"><i class="icon-slari-cancel"></i></a>').appendTo('.search-form');
        // jQuery('.search-field').removeAttr('placeholder');

        var mainContainer = jQuery('.wrapper'),
            openCtrl = jQuery('.search-icon'),
            closeCtrl = jQuery('.close-pop'),
            searchContainer = jQuery('.search-form'),
            inputSearch = jQuery('.search-field');


        function initEvents() {
            openCtrl.click(function() {
                openSearch();
            });
            closeCtrl.click(function() {
                closeSearch();
            });
            document.addEventListener('keyup', function(ev) {
                // escape key.
                if (ev.keyCode == 27) {
                    closeSearch();
                }
            });
        }

        function openSearch() {
            searchContainer.addClass('search-open');
            jQuery('body > .wrapper').addClass('body-resize');
            jQuery('body').addClass('no-scroll');
            setTimeout(function() {
                inputSearch.focus();
            }, 600);
        }

        function closeSearch() {
            searchContainer.removeClass('search-open');
            jQuery('body > .wrapper').removeClass('body-resize');
            jQuery('body').removeClass('no-scroll');
            inputSearch.blur();
            inputSearch.value = '';
        }

        initEvents();
    },

    widgets: function(){

        ;

        jQuery('.widget-area').appendTo('body');
        jQuery('<a class="close-pop"><i class="icon-slari-cancel"></i></a>').appendTo('.widget-area');
        jQuery('<h2 class="widget-header">Widgets</h2>').prependTo('.widget-area');

        var widgetBtn = jQuery('.show-widgets'), 
            widgetContainer = jQuery('.widget-area'),
            closeCtrl = jQuery('.close-pop');

        function initEvents() {
            widgetBtn.click(function(e) {
                e.preventDefault();
                toggleWidget();
            });
            closeCtrl.click(function(e) {
                e.preventDefault();
                closeWidget();
            });

        }

        function toggleWidget() {
            widgetContainer.addClass('toggle-widget');
            jQuery('body > .wrapper').addClass('body-resize');
            jQuery('body').addClass('no-scroll');
        }

        function closeWidget() {
            widgetContainer.removeClass('toggle-widget');
            jQuery('body > .wrapper').removeClass('body-resize');
            jQuery('body').removeClass('no-scroll');
        }
        initEvents();
    },

    skills: function(){
        jQuery('.skills').each(function() {
            var that = this;
            var waypoints = jQuery(this).waypoint({
                handler: function(direction) {
                    jQuery('.skill-percent-wrap').each(function() {
                        jQuery(this).find('.skill-percent').delay(10000).css({
                            width: jQuery(this).attr('data-percent')
                        });
                    });
                },
                offset: '30%'
            });
            return false;
        });
    },

    // ==============================================================================================
    // Call your function here to become a single function
    // * This method make your code more modular and make it easy to toggle your function
    // * If you want to disable a function, just commented on function that you need to disable below
    // ==============================================================================================

    init: function($) {
        script.searchForm();
        script.widgets();
        script.skills();
    },

};

jQuery(document).ready(function($) {
    "use strict";

    // This code will initialize your whole function in this JS file
    script.init($);

    jQuery(window).resize(function() {
        // Insert your JS function here that need to triggered when window resize
        // script.searchForm();
    });
});