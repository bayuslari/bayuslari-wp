** slariwp **
A Ghost-like WordPress theme

slariwp (for WordPress) is a simple yet beautiful theme for bloggers.

Inspired by the Ghost blogging platform, slariwp is a WordPress port of the default theme by the same name. The goal of this project is to emulate the gorgeous theme while taking advantage of features exclusive to the WordPress framework. There are plenty of customization options included, accessible through the WordPress Customizer. Already included are hooks to serve responsive images appropriately and media queries to provide a fast and seamless experience from desktop to mobile.
