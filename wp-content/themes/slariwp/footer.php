<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package slariwp
 */
?>
	<footer id="colophon" class="site-footer" role="contentinfo">
	    <a class="show-widgets icon-slari-settings" href="#"><span class="tooltip"><?php _e('Show widgets, please!'); ?></span></a>
		<div class="site-info inner">
		    <section class="copyright">
		    	<?php if(  false == get_theme_mod( 'slariwp_custom_footer') ) { ?>
		    		<?php printf( __( 'Made with ❤ by <a href="https://github.com/bayuslari">Bayu Slari</a>') ); ?>
		    	<?php } else { echo get_theme_mod( 'slariwp_custom_footer'); } ?>
		    </section>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</main><!-- /#content -->

<?php wp_footer(); ?>
</body>
</html>